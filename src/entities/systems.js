import {PLATFORM_START_Y, SCREEN_WIDTH} from "../../App";
import {PLATFORM_WIDTH} from "../components/platform/platform";

const MovePlatform = (entities, { touches }) => {
    let coords = touches.find(t => t.type === "move");
    if(coords){
        let platform = entities['platform'];
        if (platform && platform.position) {
            let platform_x_posiiton = platform.position[0] + coords.delta.pageX*2;
            if(isPlatformInBound(platform_x_posiiton)){
                platform.position = [
                    platform_x_posiiton,
                    PLATFORM_START_Y
                ];
            }
        }
    }
    return entities;
};

const isPlatformInBound = (platform_x_posiiton)=>{
    return (platform_x_posiiton - PLATFORM_WIDTH / 2) > 0 && (platform_x_posiiton + PLATFORM_WIDTH / 2) < SCREEN_WIDTH;
};

export { MovePlatform };