import React, {PureComponent} from 'react'
import {View} from 'react-native'
import Matter from "matter-js";

class Ball extends PureComponent{
    render(){
        return(
            <View style={{backgroundColor:'#000', width:100, height:100}}/>
        )
    }
}

export default (world,pos)=>{
    console.log(world,pos);
    let ball = Matter.Bodies.circle(pos.x,pos.y,100,100,{isStatic:false});
    Matter.World.add(world,[ball]);
    return{ball,renderer:<Ball/>}
}