import React,{PureComponent} from 'react'
import {View} from 'react-native'
import styles from './styles'

export const PLATFORM_WIDTH = 100;
export const PLATFORM_HEIGHT = 20;

export class Platform extends PureComponent{
    constructor(props){
        super(props)
    }

    render(){
        const x = this.props.position[0] - PLATFORM_WIDTH/2;
        const y = this.props.position[1];
        return(
            <View style={[styles.platform,{width:PLATFORM_WIDTH, height:PLATFORM_HEIGHT,left:x, top:y}]}/>
        )
    }
}