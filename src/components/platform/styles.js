import {StyleSheet} from 'react-native'

export default styles = StyleSheet.create({
    platform:{
        width:100,
        height:20,
        borderRadius:20,
        backgroundColor: '#635a5a'
    }
})