import React, { PureComponent } from "react";
import { StyleSheet, StatusBar } from "react-native";
import { GameEngine } from "react-native-game-engine";
import {Platform, PLATFORM_HEIGHT} from "./src/components/platform/platform";
import { MovePlatform } from "./src/entities/systems"
import Matter  from 'matter-js'
import Ball from './src/components/ball/ball'

const Dimensions = require('Dimensions');

export const SCREEN_WIDTH = Dimensions.get('window').width;
export const SCREEN_HEIGHT = Dimensions.get('window').height;

export const PLATFORM_START_X = (SCREEN_WIDTH/2);
export const PLATFORM_START_Y = (SCREEN_HEIGHT) - PLATFORM_HEIGHT*2;

export default class App extends PureComponent {
    constructor() {
        super();
        Matter.Common.isElement = () => false;
    }

    render() {
        let engine = Matter.Engine.create({enableSleeping:false});
        let world = engine.world;
        world.gravity = { x: 0, y: 2 };
        return (
            <GameEngine
                ref={"engine"}
                style={styles.container}
                systems={[MovePlatform]}
                entities={{
                    platform: { position: [PLATFORM_START_X,  PLATFORM_START_Y], renderer: <Platform />},
                    physics: { engine: engine, world: world },
                    // ball:Ball(world,{x:SCREEN_WIDTH/2,y:SCREEN_HEIGHT/2})
                }}>
                <StatusBar hidden={true} />
            </GameEngine>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "#E4E5D1"
    }
});